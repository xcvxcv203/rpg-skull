using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    public HealthBar healthBarPrefab; //HealthBar 프리팹의 참조를 저장한다.

    HealthBar healthBar; //인스턴스화한 HealthBar의 참조를 저장한다.

    private void Start()
    {
        hitPoints.value = startingHitPoints; //플레이어의 체력 startingHitPoints 값으로 시작한다

        healthBar = Instantiate(healthBarPrefab); //HealthBar 프리팹의 복사본을 인스터스화 한다

        healthBar.character = this;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("CanBePickedUp"))
        {
            Item hitObject = collision.gameObject.GetComponent<Consumable>().item;


            if (hitObject != null)
            {
                bool shouldDisappear = false; //충돌한 오브젝트를 감춰야 할지 나타내는 변수

                switch (hitObject.itemType)
                {
                    case Item.ItemType.COIN:
                        shouldDisappear = true;
                        break;

                    case Item.ItemType.HEALTH:
                       shouldDisappear = AdjustHitPoints(hitObject.quantity);
                        break;

                    default:
                        break;
                }

                if (shouldDisappear)
                {
                    collision.gameObject.SetActive(false);
                }
            }
        }
    }

    public bool AdjustHitPoints(int amount)
    {
        if (hitPoints.value < maxHitPoints) //현재 체력 값이 체력 값의 최대 혀용치보다 적은지 확인한다
        {
            hitPoints.value = hitPoints.value + amount;
            print("Adjusted hitpoints by: " + amount + ". New value: " + hitPoints.value);
            return true;
        }

        return false;
    }
}
