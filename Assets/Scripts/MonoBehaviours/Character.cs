using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    public HitPoints hitPoints; //현재 체력
    public float maxHitPoints; //최대 체력
    public float startingHitPoints; //최초 체력
}
